# Currency Rate History
This app allows you to save your exchange rate for UAH (ukrainian hryvnia).


**Information:**
* Please set the rate and date and then click the button 'Save'.
* If you unsure or made mistake, you can always change the value for a specific date.
* If you typed letters in the rate field, the page refreshes itself,  and you will have another opportunity to save data.
* Only up-to-date information is displaying on the chart( 90 days till today)
* You can set a high rate, but preferable to set the usual value (0-40 UAH per dollar), of course, if it`s not a Venezuela inflation.


![currency rate](https://i.ibb.co/Trb8wMJ/Screen-Shot-2020-09-24-at-21-59-16-fullpage.png)