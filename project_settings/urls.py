from django.urls import path
from core import views


urlpatterns = [
    path('', views.IndexPage.as_view(), name='index-page'),
]
