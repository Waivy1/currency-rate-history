from decimal import InvalidOperation

from django.core.exceptions import ValidationError
from django.db import models, IntegrityError
from datetime import date,  datetime, timedelta


class Rate(models.Model):
    # hope it's the worst case scenario
    value = models.DecimalField(max_digits=11, decimal_places=2)
    date = models.DateField(unique=True)

    @staticmethod
    def get_recent_rates(n_days=90):
        """
        This function gets a period from n_days till today from db

        :param n_days: amount of days you want to explore
        :return: a period from n_days till today
        """

        today = date.today()
        until_n_days = today - timedelta(days=n_days)
        last_n_days = Rate.objects.order_by('date').\
            filter(date__gte=until_n_days)

        return last_n_days

    @staticmethod
    def set_rate(date_to_check, value_to_check):
        try:
            rate = Rate(value=value_to_check, date=date_to_check)
            rate.save()

        except IntegrityError as e:
            rate = Rate.objects.get(date=date_to_check)
            rate.value = value_to_check
            rate.save()

            return True

        except (ValidationError, InvalidOperation) as e:
            pass

        else:
            return True

        return False

