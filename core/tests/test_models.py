from datetime import datetime

from django.test import TestCase
from core.models import Rate


class RateTest(TestCase):
    def test_get_recent_rates(self):
        expected_value = 12
        Rate.set_rate(date_to_check=datetime.now(),
                      value_to_check=expected_value)
        rate = Rate.get_recent_rates(1)
        actual = rate[0].value

        self.assertEqual(expected_value, actual)

    def test_set_rate_success(self):
        date = datetime.strptime('12-12-2202', '%d-%m-%Y')
        new_rate = Rate.set_rate(date, 1337)
        self.assertTrue(new_rate)

    def test_set_rate_wrong_value(self):
        date = datetime.strptime('11-02-3202', '%d-%m-%Y')
        new_rate = Rate.set_rate(date, 100000000000)
        self.assertFalse(new_rate)
