from datetime import datetime

from django.shortcuts import render, redirect
from django.views import View
from . import models


class IndexPage(View):
    def get(self, request):
        """
        This view is responsible for form groups where
         user can set rate and date; and showing a rate chart

        :return: main page with a rate chart
        """
        days = []
        values = []
        for rate in models.Rate.get_recent_rates(n_days=90):
            days.append(rate.date.strftime('%d.%m.%y'))
            values.append(str(rate.value))

        return render(
            request, 'index_page.html',
            {'days': days, 'values': values,
             'today': datetime.today().date().isoformat()})

    def post(self, request):
        value = request.POST['value']
        date = request.POST['date']

        if datetime.strptime(date, '%Y-%m-%d') < datetime.today():
            models.Rate.set_rate(date, value)

        return redirect('index-page')
